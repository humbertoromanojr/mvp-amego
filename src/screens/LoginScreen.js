import React from 'react';
import {Text, View, StyleSheet} from 'react-native';

export default function LoginScreen() {
  return (
    <View>
      <Text style={styles.container}>LoginScreen screen</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {flex: 1, alignItems: 'center', justifyContent: 'center'},
});
