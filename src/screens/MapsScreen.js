import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

import Maps from '../assets/images/google-maps.jpeg';
import Play from '../assets/images/home-play.png';

export default function MapsScreen({navigation}) {
  return (
    <View style={styles.container}>
      <View style={styles.containerImage}>
        <Image
          source={Maps}
          resizeMode="cover"
          style={styles.backgroundImage}
        />
      </View>

      <View style={styles.containerButton}>
        <TouchableOpacity onPress={() => navigation.navigate('MenuScreen')}>
          <Image source={Play} resizeMode="cover" style={styles.button} />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  containerImage: {
    display: 'flex',
    flex: 1,
    position: 'absolute',
  },
  backgroundImage: {
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
  },
  containerButton: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    width: 100,
    position: 'relative',
  },
});
