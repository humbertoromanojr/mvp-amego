import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  Dimensions,
  StatusBar,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

import Profile from '../assets/images/profile01-perfil.png';
import ButtonPlay from '../assets/images/profile01-selecionar.png';
import Avatar from '../assets/images/avatar.png';
import Avatar01 from '../assets/images/avatar-1.png';
import Avatar02 from '../assets/images/avatar-2.png';
import Avatar03 from '../assets/images/avatar-3.png';
import Avatar04 from '../assets/images/avatar-4.png';
import Avatar05 from '../assets/images/avatar-5.png';
import Avatar06 from '../assets/images/avatar-6.png';
import Avatar07 from '../assets/images/avatar-7.png';
import Avatar08 from '../assets/images/avatar-8.png';
import Avatar09 from '../assets/images/avatar-9.png';
import Avatar10 from '../assets/images/avatar-10.png';
import Avatar11 from '../assets/images/avatar-11.png';
import Avatar12 from '../assets/images/avatar-12.png';
import Avatar13 from '../assets/images/avatar-13.png';
import Avatar14 from '../assets/images/avatar-14.png';

export default function ProfileScreen({navigation}) {
  return (
    <View style={styles.container}>
      <StatusBar BackgroundStyle="light" backgroundColor="#f62328" />
      <View style={styles.containerImage}>
        <Image
          source={Profile}
          resizeMode="cover"
          style={styles.backgroundImage}
        />
      </View>

      <View style={styles.containerScrollView}>
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={200}
          decelerationRate="fast"
          pagingEnabled>
          <Image source={Avatar} resizeMode="contain" style={styles.avatars} />
          <Image
            source={Avatar01}
            resizeMode="contain"
            style={styles.avatars}
          />
          <Image
            source={Avatar02}
            resizeMode="contain"
            style={styles.avatars}
          />
          <Image
            source={Avatar03}
            resizeMode="contain"
            style={styles.avatars}
          />
          <Image
            source={Avatar04}
            resizeMode="contain"
            style={styles.avatars}
          />
          <Image
            source={Avatar05}
            resizeMode="contain"
            style={styles.avatars}
          />
          <Image
            source={Avatar06}
            resizeMode="contain"
            style={styles.avatars}
          />
          <Image
            source={Avatar07}
            resizeMode="contain"
            style={styles.avatars}
          />
          <Image
            source={Avatar08}
            resizeMode="contain"
            style={styles.avatars}
          />
          <Image
            source={Avatar09}
            resizeMode="contain"
            style={styles.avatars}
          />
          <Image
            source={Avatar10}
            resizeMode="contain"
            style={styles.avatars}
          />
          <Image
            source={Avatar11}
            resizeMode="contain"
            style={styles.avatars}
          />
          <Image
            source={Avatar12}
            resizeMode="contain"
            style={styles.avatars}
          />
          <Image
            source={Avatar13}
            resizeMode="contain"
            style={styles.avatars}
          />
          <Image
            source={Avatar14}
            resizeMode="contain"
            style={styles.avatars}
          />
        </ScrollView>
      </View>

      <View style={styles.containerButton}>
        <TouchableOpacity
          onPress={() => navigation.navigate('ProfileSurpriseScreen')}>
          <Image
            source={ButtonPlay}
            resizeMode="contain"
            style={styles.button}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  containerScrollView: {
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 10,
    paddingVertical: 10,
    height: 130,
    marginTop: 50,
    backgroundColor: 'rgba(255, 255, 255, 0.6)',
    borderRadius: 10,
  },
  avatars: {
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 10,
    width: 100,
  },
  containerImage: {
    display: 'flex',
    position: 'absolute',
  },
  backgroundImage: {
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
  },
  containerButton: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: 150,
  },
  button: {
    width: 200,
    position: 'relative',
  },
});
