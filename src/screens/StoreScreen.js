import React from 'react';
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

import Stores from '../assets/images/stores.jpeg';
import ButtonPlay from '../assets/images/home-play.png';

export default function StoreScreen({navigation}) {
  return (
    <View style={styles.container}>
      <View style={styles.containerImage}>
        <Image
          source={Stores}
          resizeMode="cover"
          style={styles.backgroundImage}
        />
      </View>

      <View style={styles.containerButton}>
        <TouchableOpacity
          onPress={() => navigation.navigate('StoreCartScreen')}>
          <Image source={ButtonPlay} resizeMode="cover" style={styles.button} />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  containerImage: {
    display: 'flex',
    flex: 1,
    position: 'absolute',
  },
  backgroundImage: {
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
  },
  containerButton: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    width: 100,
    position: 'relative',
  },
});
