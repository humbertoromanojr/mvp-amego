const twilio = require('twilio');

const smsClient = twilio(process.env.ACCOUNT_SID, process.env.AUTH_TOKEN);

smsClient.messages
  .create({
    from: '+12057497906', // From a valid Twilio number SMS
    to: '+5527997506668', // Text this number
    body: 'Hello from Node, MegaHack TEAM 9 :: Project AmeGo ::',
  })
  .then(message => console.log(message.sid))
  .catch(console.error);
