const twilio = require('twilio');

const whatsappClient = twilio(process.env.ACCOUNT_SID, process.env.AUTH_TOKEN);

whatsappClient.messages
  .create({
    from: 'whatsapp:+12057497906', // From a valid Twilio number SMS
    to: 'whatsapp:+5514998329612', // Text this number
    body: 'Hello from Node, MegaHack TEAM 9 :: Project AmeGo ::',
  })
  .then(message => console.log(message.sid))
  .catch(console.error);
